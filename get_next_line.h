/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yiwang <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 14:41:39 by yiwang            #+#    #+#             */
/*   Updated: 2019/05/22 13:56:19 by yiwang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 32

# include "fcntl.h"
# include "unistd.h"
# include "stdlib.h"
# include "libft/libft.h"

int		ft_fetch_line(char**s, char **line, int fd, int ret);
int		get_next_line(const int fd, char **line);

#endif
