#include <fcntl.h>
#include <stdio.h>
#include "get_next_line.h"

int main(int argc, const char *argv[])
{
	int fd;
	char *line;

	fd = open(argv[1], O_RDONLY);
	get_next_line(fd, &line);
	printf ("Line : %s\n", line);
	get_next_line(fd, &line);
	printf ("Line : %s\n", line);
	get_next_line(fd, &line);
	printf ("Line : %s\n", line);
	get_next_line(fd, &line);
	printf ("Line : %s\n", line);
	get_next_line(fd, &line);
	printf ("Line : %s\n", line);
	
return 0;
}
